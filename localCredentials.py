import os

def get_credentials():
    try:
        with open(os.path.dirname(os.path.abspath(__file__)) + '/wifi.txt', 'r') as f:
            ssid = f.readline()
            password = f.readline()
            return ssid[:-1], password
    except IOError:
        return None, None


def save_credentials(ssid, password):
    with open(os.path.dirname(os.path.abspath(__file__)) + '/wifi.txt', 'w') as f:
        f.writelines((ssid, "\n", password))