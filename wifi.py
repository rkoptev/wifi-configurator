import subprocess
import socket
import time
import os
import re

FNULL = open(os.devnull, 'w')


def connect(ssid, password):
	with open('/etc/wpa_supplicant/wpa_supplicant.conf', 'w') as f:
		f.write('country=US\nctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\n\nnetwork={\n    ssid="' + ssid + '"\n    psk="' + password + '"\n}\n')
	subprocess.call('./scripts/switchToManaged.sh', shell=True, cwd=os.path.dirname(os.path.abspath(__file__)), stdout=FNULL)#, stderr=FNULL)

def check_connection():
	# This method will "ping" 8.8.8.8
	try:
		socket.setdefaulttimeout(3)
		socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(("8.8.8.8", 53))
		return True
	except Exception as e:
		return False

def list_stations():
	output = ''
	try:
		output = subprocess.check_output('iwlist wlan0 scan', shell=True, stderr=subprocess.STDOUT)
	except Exception as e:
		time.sleep(1)
		output = subprocess.check_output('iwlist wlan0 scan', shell=True, stderr=subprocess.STDOUT)
	essid = re.findall('(?<=ESSID:").+(?=")', output)
	quality = re.findall('(?<=Quality\=)\d+', output)
	quality = map(lambda x: int(x)*100/70, quality)
	return essid, quality

def switch_to_AP_mode():
	subprocess.call('./scripts/switchToAP.sh', shell=True, cwd=os.path.dirname(os.path.abspath(__file__)), stdout=FNULL)#, stderr=FNULL)