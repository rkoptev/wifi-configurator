#!/bin/bash
# switchToManaged script
# switches the pi into client mode

echo "Stopping hostapd"
sudo service hostapd stop
echo "Stopping DNS and DHCP server"
sudo service dnsmasq stop
echo "Shutdown wlan0"
sudo ifdown --force wlan0
echo "Copying /etc/network/interfaces config for client"
sudo cp config/interfaces.managed /etc/network/interfaces
sudo cp config/dhcpcd.conf.managed /etc/dhcpcd.conf
echo "Starting wlan0"
sudo ifup wlan0
echo "Starting wpa_supplicant"
sudo service wpa_supplicant start
echo "Restart DHCP client"
sudo service dhcpcd restart