#!/bin/bash
# switchToAP script
# switches pi into ap mode

echo "Killing wpa_supplicant"
sudo pkill wpa_supplicant
echo "Shutdown wlan0"
sudo ifdown --force wlan0
echo "Copying config files for AP"
sudo cp config/interfaces.ap /etc/network/interfaces
sudo cp config/dhcpcd.conf.ap /etc/dhcpcd.conf
echo "Restart DHCP client (for ethernet)"
sudo service dhcpcd restart
echo "Starting wlan0"
sudo ifup wlan0
echo "Starting hostapd"
sudo service hostapd start
echo "Starting DNS and DHCP server"
sudo service dnsmasq start