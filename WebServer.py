from flask import Flask, request
import thread
import time
import wifi
import localCredentials

app = Flask(__name__)

class WebServer():
	__app = None

	# External flags:
	configurationAvailable = False
	configurationSSID = None
	configurationPassword = None

	def __route_root(self):
		ssid, quality = wifi.list_stations()
		response = '<!DOCTYPE html><html><head><title>Configure your PI</title></head><body><form action="connect" method="post">Select SSID:<br>'
		for i in range(len(ssid)):
			response += '<input type="radio" name="ssid" value="' + ssid[i] + '" required>' + ssid[i] + ' (signal:' + str(quality[i]) + '%)</input><br>'

		response += 'Password:<br><input type="password" name="password" required minlength=8"><br><input type="submit" value="Connect"></form></body></html>'
		return response

	def __route_connect(self):
		ssid = request.form['ssid']
		password = request.form['password']
		# Set appropriate flags
		self.configurationSSID = ssid
		self.configurationPassword = password
		self.configurationAvailable = True
		return "OK"

	def __run_app(self):
		app.route('/', methods=['GET'])(self.__route_root)
		app.route('/connect', methods=['POST'])(self.__route_connect)
		app.run(host='0.0.0.0', port=80, debug=True, use_reloader=False)

	def run(self):
		thread.start_new_thread(self.__run_app, ())