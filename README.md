0. Download and install raspbian. You can find last image at https://www.raspberrypi.org/downloads/raspbian/ . Download full image and flash it to SD card with minimum 8GB capacity. After flashing if you want to use SSH instead of keyboard and monitor, you need to create empty file with name "ssh" in boot partition on sd card. After doing this steps, you can insert sd card and boot PI. Default login is "pi", password is "raspberry".
1. Clone this repository in user directory on your PI (It's very important to clone repository exactly in home directory):
```
$ cd
$ git clone https://rkoptev@bitbucket.org/rkoptev/wifi-configurator.git
$ cd wifi-configurator
```
2. Install dependencies and configure them:
```
$ ./install.sh
```
3. Done
```
$ sudo reboot
```
This Wi-Fi service will run at every system startup. The name of access point will be "raspberry". The address of configuration page is 192.168.1.1