import time
import logging
import WebServer, wifi, localCredentials, apiClient

WIFI_CONNECTION_TIMEOUT = 15#seconds
UPDATE_INTERVAL = 3#seconds

logging.basicConfig(level=logging.INFO)#, filename=(os.path.dirname(os.path.abspath(__file__)) + '/log.log'))

# Start web server
webServer = WebServer.WebServer()
webServer.run()

def checkConnection():
	tStart = time.time()
	while True:
		if wifi.check_connection():
			logging.info("Connection to internet OK")
			return True
		elif time.time() - tStart > WIFI_CONNECTION_TIMEOUT:
			logging.warn("Can't connect to internet!")
			return False
		else:
			time.sleep(3)

def requestNewConfiguration():
	# This function will return only after connecting to wifi and checking internet
	while True:
		wifi.switch_to_AP_mode()
		while True:
			if webServer.configurationAvailable:
				# Get data and reset flag
				ssid = webServer.configurationSSID
				password = webServer.configurationPassword
				logging.info("New credentials received:")
				logging.info("SSID: " + ssid)
				logging.info("Password: " + password)
				webServer.configurationAvailable = False
				localCredentials.save_credentials(ssid, password)
				logging.info("Connecting")
				wifi.connect(ssid, password)
				break
			else:
				time.sleep(1)

		logging.info("Waiting for internet connection...")
		if checkConnection() == True:
			return
		else:
			pass
		logging.info("Waiting for new credentials...")

# Start ----------------

# Load credentials from file and try to connect
ssid, password = localCredentials.get_credentials()

if ssid != None:
	logging.info("Last known wifi credentials:")
	logging.info("SSID:" + ssid)
	logging.info("Password:" + password)
	logging.info("Connecting...")
	wifi.connect(ssid, password)
	if checkConnection() == True:
		pass
	else:
		logging.warn("Credentials are not valid, switching to AP mode...")
		requestNewConfiguration()
else:
	logging.info("Credentials not found, switching to AP mode...")
	requestNewConfiguration()

# Monitor internet connection and reconnect if needed
while True:
	if not wifi.check_connection():
		logging.warn("Internet probe failed, waiting for reconnection...")
		if checkConnection() == True:
			logging.info("Reconnect succesfull")
		else:
			logging.warn("Switching to AP mode...")
			requestNewConfiguration()

	apiResponse = apiClient.get_status()
	if apiResponse != None and apiResponse['StatusCode'] == 'Successful' and apiResponse['Status'] == 'Required configuration':
		logging.info("Got 'Required configuration' flag from API, switching to AP mode...")
		requestNewConfiguration()
	time.sleep(UPDATE_INTERVAL)
