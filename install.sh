#!/bin/bash

echo "Installing required packages"
sudo apt-get update
sudo apt-get -y install hostapd dnsmasq

echo "Configuring packages"
sudo cp config/hostapd /etc/default/hostapd
sudo cp config/hostapd.conf /etc/default/hostapd.conf
sudo cp config/dnsmasq.conf /etc/dnsmasq.conf
sudo cp config/rc.local /etc/rc.local

sudo update-rc.d hostapd disable
sudo update-rc.d dnsmasq disable

echo "Done!"